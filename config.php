<?php

$_CONFIG = array();

/********************** 
* Application Settings
**********************/

$_CONFIG["host"] = "http://localhost/";
$_CONFIG["app_root"] = "bootstrap-skeleton/";
$_CONFIG["app_url"] = $_CONFIG["host"] . $_CONFIG["app_root"];
$_CONFIG["site_root"] = realpath(dirname(__FILE__));
$_CONFIG["app_name"] = "Bootstrap Skeleton";
$_CONFIG["maintenance"] = false;


/****************
 * MySQL Settings
 ****************/

$_CONFIG["MySQL_HOST"] = "localhost";
$_CONFIG["MySQL_USER"] = "root";
$_CONFIG["MySQL_PASS"] = "";
$_CONFIG["MySQL_DATABASE"] = "bootstrap-skeleton";
$_CONFIG["MySQL_CHARSET"] = "utf8";
$_CONFIG["MySQL_PERSISTENT"] = true;
$_CONFIG["MySQL_PORT"] = "3336";
