Bootstrap Skeleton
---

This is a simple PHP Skeleton application configured for using Twitter's Bootstrap 3 API and MySQL.

Also includes small helper classes and functions, to get you going even faster. 