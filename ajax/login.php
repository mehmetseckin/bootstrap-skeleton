<?php
if($session->logged_in) {
    $session->destroy();
}

$username = (isset($_POST["kullanici_adi"])) ? $_POST["kullanici_adi"] : false;
$password = (isset($_POST["sifre"])) ? md5($_POST["sifre"]) : false;

if(!$username) {
    die("Invalid username.");
}
if(!$password) {
    die("Invalid password.");
}

$stmt = $db->prepare("SELECT k.kullanici_id, k.ad, k.soyad, g.grup_id, g.grup_adi
            FROM kullanicilar k, gruplar g
            WHERE k.kullanici_adi=?
            AND k.sifre=?
            AND k.grup_id=g.grup_id");

$stmt->execute(array($username, $password));
$result = $stmt->fetch();

if($result['kullanici_id'] > 0) {
    $session->logged_in = true;
    $session->kullanici_id = $result['kullanici_id'];
    $session->kullanici_adi = $username;
    $session->ad = $result["ad"];
    $session->soyad = $result["soyad"];
    $session->grup_id = $result["grup_id"];
    $session->grup_adi = $result["grup_adi"];
    die(true);
} else {
    $session->destroy();
    die("Kullanıcı adı veya şifre doğrulanamadı.");
}