<?php
$time = -microtime(false);

// Start the system.
include("./boot.php");

// Get header.
include("./include/header.php");

if($_CONFIG["maintenance"]) {
    include("./views/maintenance.php");
}

$page = (isset($_GET["page"])) ? $_GET["page"] : "home";
if (file_exists("./views/$page.php")) {
    include ("./views/$page.php");
} 
else {
   include ("./views/home.php");
}

include("./include/footer.php");

$time += microtime(false);
//echo "Script Loaded in : $time";
?>