<?php

class SessionManager {
    private $sessionId;
    public function __construct() {
        if(session_id() == "")
            session_start();
        $this->sessionId = session_id();
    }    
    
    public function __get($key) {
        if(isset($_SESSION[$key]))
            return $_SESSION[$key];
        else
            return null;
    }
    
    public function __set($key, $value ="") {
        $_SESSION[$key] = $value;
    }
    
    public function destroy() {
        foreach(array_keys($_SESSION) as $key) {
            unset($_SESSION[$key]);
        }
        session_destroy();
    }
    
    public function __toString() {
        return $this->sessionId;
    }
} 