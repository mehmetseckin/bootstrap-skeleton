<?php

try {
    $connection_string = 'mysql:host=' . $_CONFIG["MySQL_HOST"] . ';dbname=' . $_CONFIG["MySQL_DATABASE"] . ';charset=' . $_CONFIG["MySQL_CHARSET"] ;
    $db = new PDO($connection_string, $_CONFIG["MySQL_USER"], $_CONFIG["MySQL_PASS"], array(
        PDO::ATTR_PERSISTENT => $_CONFIG["MySQL_PERSISTENT"]
    ));
} catch (PDOException $e) {
    echo $e->getMessage();
}