<?php
// Get configuration
include("./config.php");

// Initialize requests
include("./core/request.php");

// Initialize sessions
include("./core/session.php");

// Initialize database
include("./core/database.php");

// Define helper functions
include("./core/functions.php");