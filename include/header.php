<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">        
        <title><?php echo $_CONFIG["app_name"]; ?> | Home</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="shortcut icon" href="<?php echo asset_url('favicon.ico', 'images'); ?>">
        <link rel="stylesheet" href="<?php echo asset_url('reset.css', 'css'); ?>" />        
        <link rel="stylesheet" href="<?php echo asset_url('bootstrap.css', 'css'); ?>" />
        <link rel="stylesheet" href="<?php echo asset_url('main.css', 'css'); ?>" />           
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
