

      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top.</a></p>
        <p>© 2014 <a href="<?php echo base_url(); ?>"><?php echo $_CONFIG["app_name"]; ?></a> - All rights reserved.
      </footer>

    </div><!-- /.container -->
    
        <script src="<?php echo asset_url('jquery-1.10.2.min.js', 'js'); ?>" type="text/javascript"></script>    
        <script src="<?php echo asset_url('bootstrap.min.js', 'js'); ?>" type="text/javascript"></script>    
        <script src="<?php echo asset_url('main.js', 'js'); ?>" type="text/javascript"></script>    
    </body>
</html>