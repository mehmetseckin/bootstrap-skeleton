<?php
// Start the system.
include("./boot.php");

$request = (isset($_GET["request"])) ? $_GET["request"] : "home";
if (file_exists("./ajax/$request.php")) {
    include ("./ajax/$request.php");
}
else {
    die("Undefined request.");
}
